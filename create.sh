#!/bin/bash

# dqib - Debian quick image baker
# Copyright © 2019 Giovanni Mascellani <gio@debian.org>

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set -e

SYSTEM="$1"

if [ -z "$DISK_SIZE" ] ; then
    DISK_SIZE="10G"
fi

if [ -z "$MEM" ] ; then
    MEM="1G"
fi

if [ -z "$SUITE" ] ; then
    SUITE="unstable"
fi

if [ -z "$MIRROR" ] ; then
    MIRROR="_default"
fi

DIR=`mktemp -d /tmp/$SYSTEM-XXXXXXX`
DISK="$DIR/image.qcow2"

FOUND="no"
PORTS="no"

if [ "$SYSTEM" == "i386-pc" ] ; then
    ARCH="i386"
    LINUX="linux-image-686"
    QEMU_ARCH="i386"
    QEMU_MACHINE="pc"
    QEMU_DISK="-drive file=$DISK"
    QEMU_CPU="coreduo"
    QEMU_NET_DRIVER="e1000"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "amd64-pc" ] ; then
    ARCH="amd64"
    LINUX="linux-image-amd64"
    QEMU_ARCH="x86_64"
    QEMU_MACHINE="pc"
    QEMU_CPU="Nehalem"
    QEMU_DISK="-drive file=$DISK"
    QEMU_NET_DRIVER="e1000"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "mips-malta" ] ; then
    ARCH="mips"
    LINUX="linux-image-4kc-malta"
    QEMU_ARCH="mips"
    QEMU_MACHINE="malta"
    QEMU_CPU="4KEc"
    QEMU_DISK="-drive file=$DISK"
    QEMU_NET_DRIVER="e1000"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "mipsel-malta" ] ; then
    ARCH="mipsel"
    LINUX="linux-image-4kc-malta"
    QEMU_ARCH="mipsel"
    QEMU_MACHINE="malta"
    QEMU_DISK="-drive file=$DISK"
    QEMU_CPU="4KEc"
    QEMU_NET_DRIVER="e1000"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "mips64el-malta" ] ; then
    ARCH="mips64el"
    LINUX="linux-image-5kc-malta"
    QEMU_ARCH="mips64el"
    QEMU_MACHINE="malta"
    QEMU_CPU="5KEc"
    QEMU_DISK="-drive file=$DISK"
    QEMU_NET_DRIVER="e1000"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyS0"
    FOUND="yes"
fi

# Does not work for the moment
if [ "$SYSTEM" == "armel-virt" ] ; then
    ARCH="armel"
    LINUX="linux-image-marvell"
    QEMU_ARCH="arm"
    QEMU_MACHINE="virt"
    QEMU_CPU="arm946"
    QEMU_DISK="-device virtio-blk-device,drive=hd -drive file=$DISK,if=none,id=hd"
    QEMU_NET_DRIVER="virtio-net-device"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyAMA0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "armhf-virt" ] ; then
    ARCH="armhf"
    LINUX="linux-image-armmp"
    QEMU_ARCH="arm"
    QEMU_MACHINE="virt"
    QEMU_CPU="cortex-a15"
    QEMU_DISK="-device virtio-blk-device,drive=hd -drive file=$DISK,if=none,id=hd"
    QEMU_NET_DRIVER="virtio-net-device"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyAMA0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "arm64-virt" ] ; then
    ARCH="arm64"
    LINUX="linux-image-arm64"
    QEMU_ARCH="aarch64"
    QEMU_MACHINE="virt"
    QEMU_CPU="cortex-a57"
    QEMU_DISK="-device virtio-blk-device,drive=hd -drive file=$DISK,if=none,id=hd"
    QEMU_NET_DRIVER="virtio-net-device"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyAMA0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "s390x-virt" ] ; then
    ARCH="s390x"
    LINUX="linux-image-s390x"
    QEMU_ARCH="s390x"
    QEMU_MACHINE="s390-ccw-virtio"
    QEMU_CPU="z900"
    QEMU_DISK="-drive file=$DISK"
    QEMU_NET_DRIVER="virtio-net-ccw"
    LINUX_FILENAME="vmlinuz"
    INITRD_FILENAME="initrd.img"
    CONSOLE="ttyAMA0"
    FOUND="yes"
fi

if [ "$SYSTEM" == "ppc64el-pseries" ] ; then
    ARCH="ppc64el"
    LINUX="linux-image-powerpc64le"
    QEMU_ARCH="ppc64le"
    QEMU_MACHINE="pseries"
    QEMU_CPU="power9"
    QEMU_DISK="-drive file=$DISK"
    QEMU_NET_DRIVER="e1000"
    LINUX_FILENAME="vmlinux"
    INITRD_FILENAME="initrd.img"
    CONSOLE="hvc0"
    FOUND="yes"
fi

if [ "$FOUND" != "yes" ] ; then
    echo "Could not find system type: $SYSTEM"
    exit 1
fi

if [ "$MIRROR" == "_default" ] ; then
    if [ "$PORTS" == "no" ] ; then
        MIRROR="http://deb.debian.org/debian"
    else
        MIRROR="http://deb.debian.org/debian-ports"
    fi
fi

QEMU_NET="-device $QEMU_NET_DRIVER,netdev=net -netdev user,id=net,hostfwd=tcp::2222-:22"

# Create the filesystem
eatmydata qemu-debootstrap --arch="$ARCH" --include="$LINUX",systemd,openssh-server --verbose "$SUITE" "$DIR"/chroot "$MIRROR"

# Install a simple fstab and set hostname
cp fstab "$DIR"/chroot/etc/fstab
echo "debian" > "$DIR"/chroot/etc/hostname

# Create and set passwords for root and user debian
chroot "$DIR"/chroot adduser --gecos "Debian user,,," --disabled-password debian
echo "root:root" | chroot "$DIR"/chroot chpasswd
echo "debian:debian" | chroot "$DIR"/chroot chpasswd

# Disable predictable interface naming and configure dhcp by default
ln -s /dev/null "$DIR"/chroot/etc/systemd/network/99-default.link
cp interfaces "$DIR"/chroot/etc/network/interfaces

# Recreate initrd
chroot "$DIR"/chroot update-initramfs -k all -c

# Create the qcow2 image
eatmydata virt-make-fs --format=qcow2 --size="$DISK_SIZE" --partition=gpt --type=ext4 --label=rootfs "$DIR"/chroot "$DISK"

# Extract kernel and initrd
ln -L "$DIR"/chroot/"$LINUX_FILENAME" "$DIR"/kernel
ln -L "$DIR"/chroot/"$INITRD_FILENAME" "$DIR"/initrd

# Propose a boot command line
echo "(Try to) boot with:"
echo qemu-system-"$QEMU_ARCH" -machine "$QEMU_MACHINE" -cpu "$QEMU_CPU" -m "$MEM" $QEMU_DISK $QEMU_NET -kernel "$DIR"/kernel -initrd "$DIR"/initrd -nographic -append '"'root=LABEL=rootfs console="$CONSOLE"'"'
echo "You can use Ctrl-a x to exit from QEMU"

#rm -fr "$DIR"/chroot
